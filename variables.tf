variable "keepers" {
  # See https://www.terraform.io/docs/providers/random/index.html#resource-quot-keepers-quot-
  description = "Map of values that, when changed, will trigger a new ID to be generated"
  type        = "map"
  default     = {}
}
