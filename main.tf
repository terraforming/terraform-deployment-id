resource "random_string" "simple" {
  keepers = "${var.keepers}"

  length  = 3
  upper   = false
  number  = false
  special = false
}

locals {
  deployment_id = "${random_string.simple.result}"
}
