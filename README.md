# Deployment ID Terraform module

Generate simple, easy to remember ID to group and distinguish between deployments.

## Usage

Generate Deployment ID before using other Terraform modules and pass ID as parameter.

    module "deployment" {
        source = "git::https://bitbucket.org/terraforming/terraform-deployment-id.git?ref=master"
    }

    module "something" {
        source        = "../terraform-..."
        deployment_id = "${module.deployment.id}"
        # ...
    }

## Outputs

| Name | Description |
|------|-------------|
| id | Deploymnent ID |

## Variables

| Name | Default | Description |
|------|---------|-------------|
| keepers | | Map of values that, when changed, will trigger a new ID to be generated |
